#ifndef BaseI2C_H_
#define BaseI2C_H_

#define I2C_0 "/dev/i2c-0"
#define I2C_1 "/dev/i2c-1"

namespace piRTC {


class BaseI2C{
private:
	unsigned int i2c_connection;
	unsigned int device;
	int file;
public:
	BaseI2C(unsigned int i2c_connection, unsigned int device);
	virtual int openConnection();
	virtual int write(unsigned char value);
	virtual unsigned char readOneRegister(unsigned int register_address);
	virtual unsigned char* readRegisters(int register_count,int start_location=0);
	virtual int writeRegister(unsigned int register_address, unsigned char value);
	virtual void setOrClearBit(unsigned int register_address, int bit_location, bool onoff);
	bool readBit(unsigned int register_address, int bit_location);
	virtual void closeConnection();
	virtual ~BaseI2C();
}; /* class BaseI2C */

} /* namespace piRTC */

#endif /* I2C_H_ */
