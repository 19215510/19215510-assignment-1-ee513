
#ifndef DS3231_H_
#define DS3231_H_
#include "BaseI2C.h"
#include <string>

// The DS3231 has 0x12 registers 
#define BUFFER_SIZE0 0x12

namespace piRTC {


class DS3231:protected BaseI2C{

private:
	unsigned int I2CBus, I2CAddress;
	unsigned char *reg_pointer;
public:
	DS3231(unsigned int I2CBus, unsigned int I2CAddress=0x68);
	float temperature;
	int seconds, minutes, hours, day, dayofweek, month, year;
	//std::string dayoftheweek = "Wednesday"; 
	virtual void Initial();
	virtual void printerFunction();
	virtual int bcdToDec(int b) { return (b/16)*10 + (b%16); }
	virtual float readSensor();
	// method to display the data
	virtual void displayTemp(int iterations = 600);
	virtual ~DS3231();
};

} /* namespace piRTC */

#endif /* DS3231_H_ */
