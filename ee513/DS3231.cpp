#include "DS3231.h"
#include <iostream>
#include <unistd.h>
#include <math.h>
#include <stdio.h>
#include <iomanip>
#include <string>

using namespace std;

namespace piRTC {

//From Table 19. of the ADXL345 Data sheet
#define sec         0x00 			//rtc seconds
#define min     	0x01   			//rtc minutes
#define hr          0x02   			//rtc hours
#define ddd			0x03			//rtc day of week
#define dd			0x04			//rtc day 0-31
#define mm			0x05			//rtc month
#define yy			0x06			//rtc year
#define ALARM1_M1  	0x07  			//rtc alarms seconds
#define ALARM1_M2	0x08   			//rtc alarms mins
#define ALARM1_M3  	0x09  			//rtc alarm hours
#define ALARM1_M4	0x0A   			//rtc alarm day/date
#define ALARM2_M1  	0x0B  			//rtc alarms mins
#define ALARM2_M2	0x0C   			//rtc alarms hours
#define ALARM2_M3  	0x0D  			//rtc alarm days
#define CTRL_REG	0x0E			//rtc control register
#define CTRL_STAT	0x0F   			//rtc control/status register
#define temp_MSB  	0x11  			//rtc temp msb
#define temp_LSB	0x12   			//rtc temp lsb
#define LED_SOLID	0b00011111		//write binary code for solid SQW output
#define LED_1_HZ	0b00100000		//write binary in registers for 1hz SQW


// constructor for the DS3231
DS3231::DS3231(unsigned int I2CBus, unsigned int I2CAddress):
	BaseI2C(I2CBus, I2CAddress), reg_pointer(NULL), temperature (0) { 
		this->I2CAddress = I2CAddress;
		this->I2CBus = I2CBus;}

// A mwthod to initialise a number of parameters and registers
// time, date and alarms, among other things are set and done here
void DS3231::Initial(){
	writeRegister(sec, 0x52);
	writeRegister(min, 0x20);
	writeRegister(hr, 0x20);
	writeRegister(dd, 0x20);
	writeRegister(mm, 0b00000011);
	writeRegister(yy, 0x20);
	// small segment to test for internet connection, the return is 0 for
	// good response, 1 or 2 for error
	int internetcon = system("ping google.com -c 1");
	if (internetcon == 0 ){
			cout << "Internet Connection available" << endl;}
	else {cout << "No internet connecion." << endl; }
	
	// setting alarm 1 to seconds after time set
	writeRegister(ALARM1_M1, 0x56);
	writeRegister(ALARM1_M2, 0x20);
	writeRegister(ALARM1_M3, 0x20);
	writeRegister(ALARM1_M4, 0x20);	
	writeRegister(CTRL_REG, LED_SOLID);
	writeRegister(CTRL_STAT, 0b10001000);		//reset alarm flags
	cout<< 
	"Alarm 1 set in 4 seconds ,and alarm 2 set 4 seconds after alarm 1"
	<<endl;
	
	//setting alarm 2 1 minute after time
	writeRegister(ALARM2_M1, 0x21);
	writeRegister(ALARM2_M2, 0x20);
	writeRegister(ALARM2_M3, 0x20);
	
}


float DS3231::readSensor(){
	this->reg_pointer = this->readRegisters(BUFFER_SIZE0, 0x00);
	this->seconds = bcdToDec(*(reg_pointer+sec));
	this->minutes = bcdToDec(*(reg_pointer+min));
	this->hours = bcdToDec(*(reg_pointer+hr));
	this->dayofweek = bcdToDec(*(reg_pointer+ddd));
	this->day = bcdToDec(*(reg_pointer+dd));
	this->month = bcdToDec(*(reg_pointer+mm));
	this->year = bcdToDec(*(reg_pointer+yy));
	// A two's complement repersentation of the temperature is present 
	// in the MSB
	int tempintportion = *(reg_pointer+temp_MSB);
	//shift LSB by 6 as the decimal places are the upper two bits of the
	//upper nibble in the LSB
	float tempdecimal = (*(reg_pointer+temp_LSB))>>6;
	// resolution of LSB is 0.25°C
	this->temperature = tempintportion+(tempdecimal*0.25);
	
	return 0;
}


void DS3231::displayTemp(int iterations){
	int count = 0;
	while(count < iterations){
	    printerFunction();
		if (this->readBit(CTRL_STAT, 0) == 1){
			writeRegister(CTRL_REG, LED_1_HZ);	//makes LED blink
			// wrapping square wave functionality (utilising sqw pin
			// as square wave
			this->setOrClearBit(CTRL_STAT, 0, 0);
			cout<<"\n Alarm 1 went - LED now Flashing" << endl;}
		if (this->readBit(CTRL_STAT, 1) == 1){ 
			writeRegister(CTRL_REG, LED_SOLID);	//makes LED ligh solid
			this->setOrClearBit(CTRL_STAT, 1, 0);
			cout << "\n Alarm 2 i guess - LED back to solid \n" << flush;}
			else{}
		 
		if (count == 0){Initial();printerFunction();cout<< " --> Initial" <<endl;}
		cout << "  \r" << flush;
	      usleep(100000);
	      count++;
	}
}

void DS3231::printerFunction(){
		this->readSensor();
	
		cout << "Temperature: ";
	    cout << fixed << setprecision(2) << this->temperature 
	    <<"°C  " <<flush;
	 
	    cout << "Date and Time: ";
	    cout << setw(2) << setfill('0') << this->hours<<":"<<flush;
		cout << setw(2) << setfill('0') << this->minutes<<":"<<flush;
		cout << setw(2) << setfill('0') << this->seconds<<"  "<<flush;
		//cout << setw(9) << setfill(' ') << this->dayoftheweek << "  ";
		cout << "20" << setw(2) << setfill('0') << this->year<<"-"<<flush;
		cout << setw(2) << setfill('0') << this->month<<"-"<<flush;
		cout << setw(2) << setfill('0') << this->day <<flush;
}

DS3231::~DS3231() {}

} /* namespace piRTC */
