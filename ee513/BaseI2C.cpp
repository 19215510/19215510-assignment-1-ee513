#include "BaseI2C.h"
#include <iostream>
#include <sstream>
#include <fcntl.h>
#include <stdio.h>
#include <iomanip>
#include <unistd.h>
#include <sys/ioctl.h>
#include <linux/i2c.h>
#include <linux/i2c-dev.h>
#include <string>

using namespace std;

namespace piRTC {

// constructor for I2CDevice
BaseI2C::BaseI2C(unsigned int i2c_connection, unsigned int device):
i2c_connection(i2c_connection), device(device){
	this->file=-1;
	this->openConnection();
}

// start communication with a bus device
int BaseI2C::openConnection(){
   string name;
   if(this->i2c_connection==0) name = I2C_0;
   else name = I2C_1;

  this->file=open(name.c_str(), O_RDWR);
   ioctl(this->file, I2C_SLAVE, this->device);
   return 0;
}

// to read a number of registers depending on how many defined
unsigned char* BaseI2C::readRegisters(int register_count, int start_location){
	this->write(start_location);
	unsigned char* data = new unsigned char[register_count];
	 read(this->file, data, register_count);
	return data;
}

// write a single byte to a defined register
int BaseI2C::writeRegister(unsigned int register_address, unsigned char value){
   unsigned char buffer[2];
   buffer[0] = register_address;
   buffer[1] = value;
   // scope resolution operator (::) still required for write as there is
   // a user defined write function as part of the namespace and class
   ::write(this->file, buffer, 2);
   return 0;
}

// a method to set or clear a specific bit in a register
void BaseI2C::setOrClearBit(unsigned int register_address, int bit_location, bool onoff){
   short reg_new = this->readOneRegister(register_address);
   if (onoff == 0){
      reg_new &= ~(1UL << bit_location);
      this->writeRegister(register_address, reg_new);}
   if (onoff == 1){
      reg_new |= 1UL << bit_location;
      this->writeRegister(register_address, reg_new);}
}

// read the value of a bit in a specific register
bool BaseI2C::readBit(unsigned int register_address, int bit_location){
   unsigned short reg_current = this->readOneRegister(register_address);
   unsigned short reg_edit = reg_current << (15 - bit_location);
   return (reg_edit = reg_edit >> 15);
   }

// method to change a single value in the device of consideration
int BaseI2C::write(unsigned char value){
   unsigned char buffer[1];
   buffer[0]=value;
   ::write(this->file, buffer, 1);
   return 0;
}

// reading a single register from the i2c device
unsigned char BaseI2C::readOneRegister(unsigned int register_address){
   this->write(register_address);
   unsigned char buffer[1];
  read(this->file, buffer, 1);
   return buffer[0];
}


// closing the file
void BaseI2C::closeConnection(){
	close(this->file);
	this->file = -1;
}

// Making a destructor
BaseI2C::~BaseI2C() {
	if(file!=-1) this->closeConnection();
}

} /* namespace piRTC */
